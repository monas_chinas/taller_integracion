<!-- Cargar estilos de la vista -->
<link rel="stylesheet" type="text/css" href="css/app.css">
<!-- Contenido de la vista -->
<div id="marketplace_step_one" class="sc-ui-layout">
    <div id="step_one_content" class="sc-ui-layout sc-ui-layout--lite sc-ui-layout sc-ui-layout--lite--main-brick">
        <div class="sc-ui-sticky" data-sticky="true">
            <div class="sc-ui-wizard">
                <div class="sc-ui-wizard__center-container"><span class="sc-ui-wizard__current-step">Paso 1</span>
                    <div class="sc-ui-wizard__container">
                        <div class="sc-ui-wizard__circle sc-ui-wizard__circle--active"></div>
                        <div class="sc-ui-wizard__lines-container">
                            <div class="sc-ui-wizard__lines">
                                <div class="sc-ui-wizard__line"></div>
                            </div>
                        </div>
                        <div class="sc-ui-wizard__circle"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sticky-sibling"></div>
        <div class="sc-ui-card-header-title syi-header-page__marketplace_step_one">
            <div class="sc-ui-card-header-title-group">
                <div>
                    <h2 class="sc-ui-card-header-title__title">Empecemos completando algunos datos</h2></div>
            </div>
            <div class="sc-ui-card-header-title__image"><img src="img/zapatilla.svg"></div>
        </div>
        <form id="title_task">
            <div class="sc-ui-card sc-ui-card sc-ui-card--big sc-ui-card--default" id="title_task">
                <div class="sc-ui-card-header__container">
                    <div class="sc-ui-card-header-title">
                        <div class="sc-ui-card-header-title-group">
                            <div>
                                <h2 class="sc-ui-card-header-title__title">Primero escribe el título de tu publicación</h2></div>
                            <p>Indica producto, marca y modelo para que los compradores sepan qué vendes.</p>
                        </div>
                    </div>
                </div>
                <div class="sc-ui-card-body">
                    <div>
                        <label class="andes-form-control andes-form-control--textfield andes-form-control--countdown syi-textfield-title">
                            <div class="andes-form-control__control">
                                <input currentnavigation="page_breadcrumb,header_page,title_task" sessionid="501937839-list4-3b122b76746b" currentstep="marketplace_step_one" placeholder="Ej: Celular Samsung Galaxy S9 64 GB negro" required="" output="list4-0.item.title" cardid="title_task" continuebrick="title_footer" confirmedtaskoutput="temp.confirmed_task_id" outputswithconsequences="" metrics="[object Object]" serverstepid="step_one" connections="[object Object]" config="[object Object]" fragment="[object Object]" match="[object Object]" location="[object Object]" history="[object Object]" childinfo="[object Object]" disabledclassname="" cardversion="0" id="title" data-hj-whitelist="true" rows="1" class="andes-form-control__field" maxlength="60" value="">
                            </div>
                            <div class="andes-form-control__border"></div><span class="andes-form-control__message"></span><span class="andes-form-control__countdown">0 / 60</span></label>
                    </div>
                    <div class="sc-ui-card-footer sc-ui-card-footer--align-right">
                        <button type="submit" class="andes-button andes-button--outline andes-button--small"><span class="andes-button__content">Continuar</span></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>