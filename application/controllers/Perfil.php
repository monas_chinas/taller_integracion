<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends CI_Controller {

	function __construct(){
    parent::__construct();
    $this->load->model('MdPerfil');
    $this->load->model('MdRegistrationAddress');
    }
	public function index()
	{
		// Recibir el id del usuario a buscar
		$intUsuarioId = $this->input->get('id');
		// Si se asignó un id del usuario, buscar sus datos
		if($intUsuarioId)
		{
			// Buscar el usuario por su id
			$datos['usuario'] = $this->MdPerfil->BuscarUsuarioId($intUsuarioId)[0];
			if( $this->MdRegistrationAddress->BuscarDireccionId($intUsuarioId)!=NULL)
			{
				$datos['direccion'] = $this->MdRegistrationAddress->BuscarDireccionId($intUsuarioId)[0];
			}
			else
			{
				$datos['direccion'] = null;
			}
			// Almacenar vista "perfil" en arrDatos
			$arrDatos['strContenido'] = $this->load->view('perfil', $datos, TRUE);
			// Cargar vista pantalla_inicial con el contenido de la vista
			$this->load->view('pantalla_inicial', $arrDatos, FALSE);
		}
		// Si no se especificó el id, regresar a la página inicial
		else
		{
			redirect('Inicio');
		}
	}

	public function VistaModificarUsuario()
	{
		// Recibir el id del usuario a buscar
		$intUsuarioId = $this->input->get('id'); 
		// Si se asignó un id del usuario, buscar sus datos
		if($intUsuarioId)
		{
			// Buscar el usuario por su id
			$datos['usuario'] = $this->MdPerfil->BuscarUsuarioId($intUsuarioId)[0];
			// Almacenar vista "modificarUsuario" en arrDatos
			$arrDatos['strContenido'] = $this->load->view('modificarUsuario', $datos, TRUE);
			// Cargar vista pantalla_inicial con el contenido de la vista
			$this->load->view('pantalla_inicial', $arrDatos, FALSE);
		}
		// Si no se especificó el id, regresar a la página inicial
		else
		{
			redirect('Inicio');
		}
	}
	

	public function ModificarUsuario()
	{
		$id = $this->input->post('id');
		$StrUsuario = $this->input->post('strUsuario');
		$intResultado = $this->MdPerfil->MdModificarUsuario($id,$StrUsuario);

		if($intResultado == 1)
		{
			$data = array(
					'id' => $this->session->userdata('id'),
					'email' => $this->session->userdata('email'),
					'nombre' => $this->input->post('strUsuario'),
					'apellido' => $this->session->userdata('apellido'),
					'rfc' => $this->session->userdata('rfc'),
					'razonSocial' => $this->session->userdata('razonSocial'),
				);
			$this->session->set_userdata($data);
			redirect('Inicio');
			
		} 
		 // Si no se especificó el id, regresar a la página inicial
		else
		{
			redirect('Inicio');
		}
	}

	public function VistaModificarEmail()
	{
		// Recibir el id del usuario a buscar
		$intUsuarioId = $this->input->get('id'); 
		// Si se asignó un id del usuario, buscar sus datos
		if($intUsuarioId)
		{
			// Buscar el usuario por su id
			$datos['usuario'] = $this->MdPerfil->BuscarUsuarioId($intUsuarioId)[0];
			// Almacenar vista "modificarUsuario" en arrDatos
			$arrDatos['strContenido'] = $this->load->view('modificarEmail', $datos, TRUE);
			// Cargar vista pantalla_inicial con el contenido de la vista
			$this->load->view('pantalla_inicial', $arrDatos, FALSE);
		}
		// Si no se especificó el id, regresar a la página inicial
		else
		{
			redirect('Inicio');
		}
	}

	public function ActualizarEmail()
	{
		$id = $this->input->post('id');
		$StrEmail = $this->input->post('strEmail');
		$intResultado = $this->MdPerfil->ActualizarEmail($id,$StrEmail);

		if($intResultado == 1)
		{
		  
		 redirect('Inicio');
			
		} 
		 else {
			redirect('Inicio');
		 }
	}
	public function VistaModificarDireccion()
	{
		// Recibir el id del usuario a buscar
		$intUsuarioId = $this->input->get('id'); 
		// Si se asignó un id del usuario, buscar sus datos
		if($intUsuarioId)
		{
			// Buscar el usuario por su id
			$datos['direccion'] = $this->MdRegistrationAddress->BuscarDireccionId($intUsuarioId)[0];
			// Almacenar vista "modificarUsuario" en arrDatos
			$arrDatos['strContenido'] = $this->load->view('modificarDireccion', $datos, TRUE);
			// Cargar vista pantalla_inicial con el contenido de la vista
			$this->load->view('pantalla_inicial', $arrDatos, FALSE);
		}
		// Si no se especificó el id, regresar a la página inicial
		else
		{
			redirect('Inicio');
		}
	}
	public function ModificarDireccion()
	{
		$strIdUsuario = $this->session->userdata('id');
				$StrNombre_apellido = $this->input->post('StrNombre_apellido');      
			    $StrCodigo_postal = $this->input->post('StrCodigo_postal');
			    $StrEstado = $this->input->post('StrEstado');
			    $StrMunicipio = $this->input->post('StrMunicipio');
			    $StrColonia = $this->input->post('StrColonia');
			    $StrCalle = $this->input->post('StrCalle');
			    $StrNumero_exterior = $this->input->post('StrNumero_exterior');
			    $StrNumero_interior = $this->input->post('StrNumero_interior');
			    $StrEntre_calles = $this->input->post('StrEntre_calles');
			    $StrReferencias = $this->input->post('StrReferencias');
			    $StrTelefono = $this->input->post('StrTelefono');
		 $intResultado = $this->MdRegistrationAddress->MdModificarDireccion($strIdUsuario,$StrNombre_apellido, $StrCodigo_postal, $StrEstado, $StrMunicipio, $StrColonia, $StrCalle, $StrNumero_exterior, $StrNumero_interior, $StrEntre_calles,$StrReferencias,$StrTelefono);

		if($intResultado == 1)
		{
		  
		 redirect('Inicio');
			
		} 
		else {
			redirect('Inicio');
		 }

	}

	public function ModificarRazonSocial()
	{
		$id = $this->input->post('id');
		$strRazonSocial = $this->input->post('strRazonSocial');
		$intResultado = $this->MdPerfil->MdModificarRazonSocial($id,$strRazonSocial);

		if($intResultado == 1)
		{
			$data = array(
					'id' => $this->session->userdata('id'),
					'email' => $this->session->userdata('email'),
					'nombre' => $this->session->userdata('nombre'),
					'apellido' => $this->session->userdata('apellido'),
					'rfc' => $this->session->userdata('rfc'),
					'razonSocial' => $this->input->post('strRazonSocial'),
				);
			$this->session->set_userdata($data);
			redirect('Inicio');
			
		} 
		 // Si no se especificó el id, regresar a la página inicial
		else
		{
			redirect('Inicio');
		}
	}
	public function VistaModificarRazonSocial()
	{
		// Recibir el id del usuario a buscar
		$intUsuarioId = $this->input->get('id'); 
		// Si se asignó un id del usuario, buscar sus datos
		if($intUsuarioId)
		{
			// Buscar el usuario por su id
			$datos['usuario'] = $this->MdPerfil->BuscarUsuarioId($intUsuarioId)[0];
			// Almacenar vista "modificarUsuario" en arrDatos
			$arrDatos['strContenido'] = $this->load->view('modificarRazonSocial', $datos, TRUE);
			// Cargar vista pantalla_inicial con el contenido de la vista
			$this->load->view('pantalla_inicial', $arrDatos, FALSE);
		}
		// Si no se especificó el id, regresar a la página inicial
		else
		{
			redirect('Inicio');
		}
	}
}